# -*- coding: utf-8 -*-
"""
Created on Wed Sep 17 22:01:43 2014

@author: Sokka
"""
from tkinter import *

# make window

tk = Tk()

tk.title("Calculator") #Window name
tk.geometry("100x100")#Window size

app = Frame(tk)
app.grid()

button1 = Button(app, text = "Button") #button plus text
button1.grid()

button2 = Button(app)
button2.grid()
button2.configure(text = "Button 2") #button 2 plus text

tk.mainloop()