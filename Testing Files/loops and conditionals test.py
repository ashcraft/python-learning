# -*- coding: utf-8 -*-
"""
Created on Wed Sep 17 17:31:34 2014

@author: Sokka
"""

#LOOPS
a = 0
while a < 10:
    a = a + 1
    print (a)
    
wait = input("enter")


#CONDITIONALS

x = 0
#IF
while x < 100:
    if x == 0:
        x = x + 5
        print (x)
    if x == 5:
        x = x * 5
        print (x)
    if x == 25:
        x = 100
        print (x)
wait = input("enter")
#ELSE
k = 100

if k < 4:
    print ("Nope")
else:
    print("k is 100")        