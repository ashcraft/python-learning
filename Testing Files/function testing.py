# -*- coding: utf-8 -*-
"""
Created on Wed Sep 17 17:51:20 2014

@author: Sokka
"""

#press enter for the input line to show.
print("Type something in!")
#input("Text") is just the text that tells the user what to do.
#it doesn't output with what you type
a = input("Input: ")

print(a)