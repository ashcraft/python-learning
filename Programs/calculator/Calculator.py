from tkinter import *
import tkinter as tk
global choice 
choice = 0


 #################################              
def calculate(*event):
    if choice == 1:
        add1 = int(calc.calcbox1.get())
        add2 = int(calc.calcbox2.get())
        answ = add1 + add2         
        answer = Label(calc, text = answ)
        answer.grid(row=1, column=0)
    elif choice == 2:
        sub1 = int(calc.calcbox1.get())
        sub2 = int(calc.calcbox2.get())
        answ = sub1 - sub2         
        answer = Label(calc, text = answ)
        answer.grid(row=1, column=0)
    elif choice == 3:
        mul1 = int(calc.calcbox1.get())
        mul2 = int(calc.calcbox2.get())
        answ = mul1 * mul2         
        answer = Label(calc, text = answ)   
        answer.grid(row=1, column=0)
    elif choice == 4:
        div1 = int(calc.calcbox1.get())
        div2 = int(calc.calcbox2.get())
        answ = div1 / div2         
        answer = Label(calc, text = answ)
        answer.grid(row=1, column=0)
def choice1():
    global choice
    choice = 1  
    calc.welcome.config(text="Addition")
def choice2():
    global choice
    choice = 2   
    calc.welcome.config(text="Subtraction")
def choice3():
    global choice
    choice = 3   
    calc.welcome.config(text="Multiplication")
def choice4():
    global choice
    choice = 4   
    calc.welcome.config(text="Division")   
def OnValidate(d, i, P, s, S, v, V, W):
        return S.isdigit()
def abouthelp():
    about = Toplevel()
    about.title = ("About")
    about.message = Label(about, text="Calculator")
    about.message2 = Label(about, text="Version .11")
    about.message3 = Label(about, text="Created by Ashcraft")
    about.message.pack()
    about.message2.pack()
    about.message3.pack()
    button = Button(about, text="Dismiss", command=about.destroy)
    button.pack()
#####################################
    

calc = Tk()
calc.title("Calculator")
calc.geometry("200x140")



if choice == 0:
    calc.welcome = Label(calc, text="Select a choice")
val = (calc.register(OnValidate),
      '%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')  
calc.calcbox1 = Entry(calc, validate="key", 
                              validatecommand=val)
calc.calcbox2 = Entry(calc, validate="key", 
                              validatecommand=val)
calc.submit = Button(calc, text="CALCULATE", command = calculate)

calc.welcome.grid(row=0,column=0)
calc.calcbox1.grid(row=2, column=0)
calc.calcbox2.grid(row=3, column=0)
calc.submit.grid(row=4, column=0)
calc.bind('<Return>', calculate)



calc.menu=Menu(calc)

filemenu = Menu(calc.menu,tearoff=0)
filemenu.add_command(label="Add", command = choice1)
filemenu.add_command(label="Subtract", command = choice2)
filemenu.add_command(label="Multiply", command = choice3)
filemenu.add_command(label="Divide", command = choice4)

calc.menu.add_cascade(label="Operations",menu=filemenu)

help = Menu(calc.menu,tearoff=0)
help.add_command(label="About", command = abouthelp)

calc.menu.add_cascade(label="Help",menu=help)
 

calc.config(menu=calc.menu)
calc.app = Frame(calc)
calc.app.grid()
calc.mainloop()  
