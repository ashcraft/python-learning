# -*- coding: utf-8 -*-
"""
Created on Thu Sep 18 09:40:04 2014

@author: Sokka
"""

def menu():
    print("Calculator")
    print(" ")
    print("1: Addition")
    print("2: Subtraction")
    print("3: Multiplication")
    print("4: Division")
    print("5: Quit")
    print(" ")
    try:
        return int(input ("Enter a option: "))
    except:
        print("You must enter a number")
        input("Press ENTER")
def add(a,b):
    print (a + b)
    
def sub(a,b):
    print (a - b)

def mul(a,b):
    print (a * b)

def div(a,b):
    print (a / b)

loop = 1
choice = 0

while loop == 1:
    choice = menu()
    if choice == 1:
        try:        
            add(int(input("First Number: ")), int(input("Second Number: ")))
        except:
            print("You must input a number.")
            input("Press ENTER")
            continue
        
    elif choice == 2:
        try:        
            sub(int(input("First Number: ")), int(input("Second Number: ")))
        except:
            print("You must input a number.")
            input("Press ENTER")
            continue
        
    elif choice == 3:
        try:        
            mul(int(input("First Number: ")), int(input("Second Number: ")))
        except:
            print("You must input a number.")
            input("Press ENTER")
            continue
        
    elif choice == 4:
        try:        
            div(int(input("First Number: ")), int(input("Second Number: ")))
        except:
            print("You must input a number.")
            input("Press ENTER")
            continue
        
    elif choice == 5:
        print("Goodbye")
        input("Press ENTER")
        loop = 0
    else:
        print("You must select a number from the given options")
        input("Press ENTER")
        continue